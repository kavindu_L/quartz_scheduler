name := "quartz_scheduler"

version := "1.0.0"

scalaVersion := "2.12.7"

libraryDependencies += "org.quartz-scheduler" % "quartz" % "2.3.0"

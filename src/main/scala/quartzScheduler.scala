import org.quartz.{CronScheduleBuilder, JobBuilder, SimpleScheduleBuilder, TriggerBuilder}
import org.quartz.impl.StdSchedulerFactory

object quartzScheduler{

  def main(args: Array[String]): Unit = {


    // create schedule instance
    val scheduler = StdSchedulerFactory.getDefaultScheduler()
    scheduler.start()

    // define the job and tie it
    val job = JobBuilder
      .newJob()
      .ofType(classOf[ScheduleJob])
      .withIdentity("myScheduleJob","group1")
      .build()

    // Trigger the job to run,every minute
    val trigger = TriggerBuilder
      .newTrigger()
      .withIdentity("myTrigger", "group1")
      .withSchedule(CronScheduleBuilder
        .cronSchedule("0 * * ? * *"))
      .build()


    /**
    // Trigger the job to run now, and then every 30 minute
    val trigger = TriggerBuilder
      .newTrigger()
      .withIdentity("myTrigger", "group1")
      .startNow()
      .withSchedule(SimpleScheduleBuilder
        .simpleSchedule()
        .withIntervalInMinutes(1)
        .repeatForever())
      .build()
     */

    // Tell quartz to schedule the job using our trigger
    scheduler.scheduleJob(job,trigger)

  }

}

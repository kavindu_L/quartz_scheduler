import java.time.LocalDateTime

import org.quartz.{Job, JobExecutionContext}

class ScheduleJob extends Job {

  override def execute(context: JobExecutionContext): Unit = {
    val time = LocalDateTime.now()
    println("Hi there, I was executed at : "+time)
  }
}
